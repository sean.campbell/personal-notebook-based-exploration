# Exploration
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

Common repository for playing around with various datasets and improving knowledge with various domains and frameworks.

### Getting Started 🎬
### Tooling 🛠
- [conda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html): Conda/poetry/pipenv/virtualenv for dependency management
- [pre-commit](https://pre-commit.com): Git hooks for consistency
- [docker](https://docs.docker.com/get-docker/): Orbstack/whatever for running on images instead

```bash
#!/usr/bin/env bash
pipx install ".[lint]" # Or brew / choco / snap
pre-commit install      # Sets up hooks that stop you pushing without passing checks
pre-commit run -a       # Before commit
```

Use either a local setup with your typical env manager or I can recommend the Jupyter docker stacks:
`docker pull jupyter/datascience-notebook:python-3.10`

On Sagemaker, I typically make use of one of the Torch images and this can be worked on locally by:
```bash
#!/usr/bin/env bash
aws ecr get-login-password --region eu-west-1 | docker login --username AWS --password-stdin 763104351884.dkr.ecr.eu-west-1.amazonaws.com
# Can use [cpu|gpu]
docker pull 763104351884.dkr.ecr.eu-west-1.amazonaws.com/pytorch-training:2.1.0-cpu-py310-ubuntu20.04-ec2
```
